import{_ as t,M as i,p as l,q as r,R as n,t as a,N as e,a1 as c}from"./framework-61fce562.js";const o={},d=n("h1",{id:"git加速",tabindex:"-1"},[n("a",{class:"header-anchor",href:"#git加速","aria-hidden":"true"},"#"),a(" git加速")],-1),p=n("h2",{id:"国内常用加速地址",tabindex:"-1"},[n("a",{class:"header-anchor",href:"#国内常用加速地址","aria-hidden":"true"},"#"),a(" 国内常用加速地址")],-1),u={href:"https://hub.yzuu.cf/",target:"_blank",rel:"noopener noreferrer"},h={href:"https://hub.nuaa.cf/",target:"_blank",rel:"noopener noreferrer"},m=c(`<h2 id="加速clone" tabindex="-1"><a class="header-anchor" href="#加速clone" aria-hidden="true">#</a> 加速clone</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment"># 方法一：手动替换地址</span>
<span class="token comment">#原地址</span>
<span class="token function">git</span> clone https://github.com/xxx/xxx.git
<span class="token comment">#改为</span>
<span class="token function">git</span> clone https://hub.yzuu.cf/xxx/xxx.git


<span class="token comment"># 方法二：配置git自动替换</span>
<span class="token function">git</span> config <span class="token parameter variable">--global</span> url.<span class="token string">&quot;ssh://git@ssh.github.com:443&quot;</span>.insteadOf https://github.com
<span class="token comment"># 测试</span>
<span class="token function">git</span> clone https://github.com/xxx/xxx.git
<span class="token comment"># 查看git配置信息</span>
<span class="token function">git</span> config <span class="token parameter variable">--global</span> <span class="token parameter variable">--list</span>
<span class="token function">git</span> config <span class="token parameter variable">--global</span> <span class="token parameter variable">-l</span>
<span class="token comment"># 取消设置</span>
$ <span class="token function">git</span> config <span class="token parameter variable">--global</span> <span class="token parameter variable">--unset</span> url.<span class="token string">&quot;ssh://git@ssh.github.com:443&quot;</span>.insteadOf
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="加速release" tabindex="-1"><a class="header-anchor" href="#加速release" aria-hidden="true">#</a> 加速release</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment"># 原地址</span>
<span class="token function">wget</span> https://github.com/goharbor/harbor/releases/download/v2.0.2/harbor-offline-installer-v2.0.2.tgz
<span class="token comment"># 加速下载方法一</span>
<span class="token function">wget</span> https://download.fastgit.org/goharbor/harbor/releases/download/v2.0.2/harbor-offline-installer-v2.0.2.tgz
<span class="token comment"># 加速下载方法二</span>
<span class="token function">wget</span> https://hub.fastgit.org/goharbor/harbor/releases/download/v2.0.2/harbor-offline-installer-v2.0.2.tgz
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="加速-raw" tabindex="-1"><a class="header-anchor" href="#加速-raw" aria-hidden="true">#</a> 加速 raw</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment"># 原地址</span>
<span class="token function">wget</span> https://raw.githubusercontent.com/xxx/xxx/master/README.md
<span class="token comment"># 加速下载方法一</span>
<span class="token function">wget</span> https://raw.staticdn.net/xxx/xxx/master/README.md
<span class="token comment"># 加速下载方法二</span>
<span class="token function">wget</span> https://raw.fastgit.org/xxx/xxx/master/README.md
<span class="token comment"># 加速下载方法</span>
<span class="token function">wget</span> https://raw.gitmirror.com/xxx/xxx/master/README.md
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="全局设置-不推荐" tabindex="-1"><a class="header-anchor" href="#全局设置-不推荐" aria-hidden="true">#</a> 全局设置（不推荐）</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment">#使用http代理 </span>
<span class="token function">git</span> config <span class="token parameter variable">--global</span> http.proxy https://github.ur1.fun
<span class="token function">git</span> config <span class="token parameter variable">--global</span> https.proxy https://github.ur1.fun
<span class="token comment">#使用socks5代理</span>
<span class="token function">git</span> config <span class="token parameter variable">--global</span> http.proxy socks5://github.ur1.fun
<span class="token function">git</span> config <span class="token parameter variable">--global</span> https.proxy socks5://github.ur1.fun
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="只对github代理-推荐" tabindex="-1"><a class="header-anchor" href="#只对github代理-推荐" aria-hidden="true">#</a> 只对Github代理（推荐）</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token comment">#使用socks5代理（推荐）</span>
<span class="token function">git</span> config <span class="token parameter variable">--global</span> http.https://github.com.proxy socks5://github.ur1.fun
<span class="token comment">#使用http代理（不推荐）</span>
<span class="token function">git</span> config <span class="token parameter variable">--global</span> http.https://github.com.proxy http://github.ur1.fun
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="取消代理" tabindex="-1"><a class="header-anchor" href="#取消代理" aria-hidden="true">#</a> 取消代理</h2><div class="language-bash line-numbers-mode" data-ext="sh"><pre class="language-bash"><code><span class="token function">git</span> config <span class="token parameter variable">--global</span> <span class="token parameter variable">--unset</span> http.proxy 
<span class="token function">git</span> config <span class="token parameter variable">--global</span> <span class="token parameter variable">--unset</span> https.proxy
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div>`,12);function b(v,g){const s=i("ExternalLinkIcon");return l(),r("div",null,[d,p,n("p",null,[n("a",u,[a("https://hub.yzuu.cf/"),e(s)])]),n("p",null,[n("a",h,[a("https://hub.nuaa.cf/"),e(s)])]),m])}const x=t(o,[["render",b],["__file","git-proxy.html.vue"]]);export{x as default};
