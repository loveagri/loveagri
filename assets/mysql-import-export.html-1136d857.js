import{_ as s,p as n,q as a,a1 as e}from"./framework-61fce562.js";const l={},o=e(`<h1 id="mysql导入导出详细教程" tabindex="-1"><a class="header-anchor" href="#mysql导入导出详细教程" aria-hidden="true">#</a> MySQL导入导出详细教程</h1><h2 id="导出" tabindex="-1"><a class="header-anchor" href="#导出" aria-hidden="true">#</a> <strong>导出</strong></h2><p>在日常维护工作当中经常会需要对数据进行导出操作，而mysqldump是导出数据过程中使用非常频繁的一个工具。</p><p><strong>语法</strong></p><div class="language-sql line-numbers-mode" data-ext="sql"><pre class="language-sql"><code>mysqldump <span class="token punctuation">[</span>OPTIONS<span class="token punctuation">]</span> <span class="token keyword">database</span> <span class="token punctuation">[</span><span class="token keyword">tables</span><span class="token punctuation">]</span>
mysqldump <span class="token punctuation">[</span>OPTIONS<span class="token punctuation">]</span> <span class="token comment">--databases [OPTIONS] DB1 [DB2 DB3...]</span>
mysqldump <span class="token punctuation">[</span>OPTIONS<span class="token punctuation">]</span> <span class="token comment">--all-databases [OPTIONS]</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p><strong>导出所有数据库</strong></p><div class="language-sql line-numbers-mode" data-ext="sql"><pre class="language-sql"><code>mysqldump <span class="token operator">-</span>uroot <span class="token operator">-</span>proot <span class="token comment">--all-databases &gt;/tmp/all.sql</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><p><strong>导出db1、db2两个数据库的所有数据</strong></p><div class="language-sql line-numbers-mode" data-ext="sql"><pre class="language-sql"><code>mysqldump <span class="token operator">-</span>uroot <span class="token operator">-</span>proot <span class="token comment">--databases db1 db2 &gt;/tmp/user.sql</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><p><strong>导出db1中的a1、a2表</strong></p><div class="language-sql line-numbers-mode" data-ext="sql"><pre class="language-sql"><code>mysqldump <span class="token operator">-</span>uroot <span class="token operator">-</span>proot <span class="token comment">--databases db1 --tables a1 a2  &gt;/tmp/db1.sql</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><p><strong>条件导出，导出db1表a1中id=1的数据</strong></p><div class="language-sql line-numbers-mode" data-ext="sql"><pre class="language-sql"><code>mysqldump <span class="token operator">-</span>uroot <span class="token operator">-</span>proot <span class="token comment">--databases db1 --tables a1 --where=&#39;id=1&#39;  &gt;/tmp/a1.sql</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><p><strong>只导出表结构不导出数据，--no-data</strong></p><div class="language-sql line-numbers-mode" data-ext="sql"><pre class="language-sql"><code>mysqldump <span class="token operator">-</span>uroot <span class="token operator">-</span>proot <span class="token comment">--no-data --databases db1 &gt;/tmp/db1.sql</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><p><strong>跨服务器导出导入数据</strong></p><div class="language-sql line-numbers-mode" data-ext="sql"><pre class="language-sql"><code>mysqldump <span class="token comment">--host=h1 -uroot -proot --databases db1 |mysql --host=h2 -uroot -proot db2</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><p><strong>所有参数说明</strong></p><div class="language-sql line-numbers-mode" data-ext="sql"><pre class="language-sql"><code><span class="token comment">--all-databases  , -A</span>
导出全部数据库。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases</span>

<span class="token comment">--all-tablespaces  , -Y</span>
导出全部表空间。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --all-tablespaces</span>

<span class="token comment">--no-tablespaces  , -y</span>
不导出任何表空间信息。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --no-tablespaces</span>

<span class="token comment">--add-drop-database</span>
每个数据库创建之前添加<span class="token keyword">drop</span>数据库语句。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --add-drop-database</span>

<span class="token comment">--add-drop-table</span>
每个数据表创建之前添加<span class="token keyword">drop</span>数据表语句。<span class="token punctuation">(</span>默认为打开状态，使用<span class="token comment">--skip-add-drop-table取消选项)</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases  (默认添加drop语句)</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases –skip-add-drop-table  (取消drop语句)</span>

<span class="token comment">--add-locks</span>
在每个表导出之前增加<span class="token keyword">LOCK</span> <span class="token keyword">TABLES</span>并且之后<span class="token keyword">UNLOCK</span>  <span class="token keyword">TABLE</span>。<span class="token punctuation">(</span>默认为打开状态，使用<span class="token comment">--skip-add-locks取消选项)</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases  (默认添加LOCK语句)</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases –skip-add-locks   (取消LOCK语句)</span>

<span class="token comment">--allow-keywords</span>
允许创建是关键词的列名字。这由表名前缀于每个列名做到。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --allow-keywords</span>

<span class="token comment">--apply-slave-statements</span>
在<span class="token string">&#39;CHANGE MASTER&#39;</span>前添加<span class="token string">&#39;STOP SLAVE&#39;</span>，并且在导出的最后添加<span class="token string">&#39;START SLAVE&#39;</span>。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --apply-slave-statements</span>

<span class="token comment">--character-sets-dir</span>
字符集文件的目录
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases  --character-sets-dir=/usr/local/mysql/share/mysql/charsets</span>

<span class="token comment">--comments</span>
附加注释信息。默认为打开，可以用<span class="token comment">--skip-comments取消</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases  (默认记录注释)</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --skip-comments   (取消注释)</span>

<span class="token comment">--compatible</span>
导出的数据将和其它数据库或旧版本的MySQL 相兼容。值可以为ansi、mysql323、mysql40、postgresql、oracle、mssql、db2、maxdb、no_key_options、no_tables_options、no_field_options等，
要使用几个值，用逗号将它们隔开。它并不保证能完全兼容，而是尽量兼容。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --compatible=ansi</span>

<span class="token comment">--compact</span>
导出更少的输出信息<span class="token punctuation">(</span>用于调试<span class="token punctuation">)</span>。去掉注释和头尾等结构。可以使用选项：<span class="token comment">--skip-add-drop-table  --skip-add-locks --skip-comments --skip-disable-keys</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --compact</span>

<span class="token comment">--complete-insert,  -c</span>
使用完整的<span class="token keyword">insert</span>语句<span class="token punctuation">(</span>包含列名称<span class="token punctuation">)</span>。这么做能提高插入效率，但是可能会受到max_allowed_packet参数的影响而导致插入失败。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --complete-insert</span>

<span class="token comment">--compress, -C</span>
在客户端和服务器之间启用压缩传递所有信息
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --compress</span>

<span class="token comment">--create-options,  -a</span>
在<span class="token keyword">CREATE</span> <span class="token keyword">TABLE</span>语句中包括所有MySQL特性选项。<span class="token punctuation">(</span>默认为打开状态<span class="token punctuation">)</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases</span>

<span class="token comment">--databases,  -B</span>
导出几个数据库。参数后面所有名字参量都被看作数据库名。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--databases test mysql</span>

<span class="token comment">--debug</span>
输出debug信息，用于调试。默认值为：d:t<span class="token punctuation">,</span><span class="token operator">/</span>tmp<span class="token operator">/</span>mysqldump<span class="token punctuation">.</span>trace
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --debug</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --debug=” d:t,/tmp/debug.trace”</span>

<span class="token comment">--debug-check</span>
检查内存和打开文件使用说明并退出。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --debug-check</span>

<span class="token comment">--debug-info</span>
输出调试信息并退出
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --debug-info</span>

<span class="token comment">--default-character-set</span>
设置默认字符集，默认值为utf8
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --default-character-set=utf8</span>

<span class="token comment">--delayed-insert</span>
采用延时插入方式（<span class="token keyword">INSERT</span> <span class="token keyword">DELAYED</span>）导出数据
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --delayed-insert</span>

<span class="token comment">--delete-master-logs</span>
master备份后删除日志<span class="token punctuation">.</span> 这个参数将自动激活<span class="token comment">--master-data。</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --delete-master-logs</span>

<span class="token comment">--disable-keys</span>
对于每个表，用<span class="token comment">/*!40000 ALTER TABLE tbl_name DISABLE KEYS */</span><span class="token punctuation">;</span>和<span class="token comment">/*!40000 ALTER TABLE tbl_name ENABLE KEYS */</span><span class="token punctuation">;</span>语句引用<span class="token keyword">INSERT</span>语句。这样可以更快地导入<span class="token keyword">dump</span>出来的文件，因为它是在插入所有行后创建索引的。该选项只适合MyISAM表，默认为打开状态。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases</span>

<span class="token comment">--dump-slave</span>
该选项将主的binlog位置和文件名追加到导出数据的文件中<span class="token punctuation">(</span><span class="token keyword">show</span> slave <span class="token keyword">status</span><span class="token punctuation">)</span>。设置为<span class="token number">1</span>时，将会以CHANGE MASTER命令输出到数据文件；设置为<span class="token number">2</span>时，会在change前加上注释。该选项将会打开<span class="token comment">--lock-all-tables，除非--single-transaction被指定。该选项会自动关闭--lock-tables选项。默认值为0。</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --dump-slave=1</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --dump-slave=2</span>

<span class="token comment">--master-data</span>
该选项将当前服务器的binlog的位置和文件名追加到输出文件中<span class="token punctuation">(</span><span class="token keyword">show</span> master <span class="token keyword">status</span><span class="token punctuation">)</span>。如果为<span class="token number">1</span>，将会输出CHANGE MASTER 命令；如果为<span class="token number">2</span>，输出的CHANGE  MASTER命令前添加注释信息。该选项将打开<span class="token comment">--lock-all-tables 选项，除非--single-transaction也被指定（在这种情况下，全局读锁在开始导出时获得很短的时间；其他内容参考下面的--single-transaction选项）。该选项自动关闭--lock-tables选项。</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --master-data=1;</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --master-data=2;</span>

<span class="token comment">--events, -E</span>
导出事件。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --events</span>

<span class="token comment">--extended-insert,  -e</span>
使用具有多个<span class="token keyword">VALUES</span>列的<span class="token keyword">INSERT</span>语法。这样使导出文件更小，并加速导入时的速度。默认为打开状态，使用<span class="token comment">--skip-extended-insert取消选项。</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases--skip-extended-insert   (取消选项)</span>

<span class="token comment">--fields-terminated-by</span>
导出文件中忽略给定字段。与<span class="token comment">--tab选项一起使用，不能用于--databases和--all-databases选项</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p test test <span class="token comment">--tab=”/home/mysql” --fields-terminated-by=”#”</span>

<span class="token comment">--fields-enclosed-by</span>
输出文件中的各个字段用给定字符包裹。与<span class="token comment">--tab选项一起使用，不能用于--databases和--all-databases选项</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p test test <span class="token comment">--tab=”/home/mysql” --fields-enclosed-by=”#”</span>

<span class="token comment">--fields-optionally-enclosed-by</span>
输出文件中的各个字段用给定字符选择性包裹。与<span class="token comment">--tab选项一起使用，不能用于--databases和--all-databases选项</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p test test <span class="token comment">--tab=”/home/mysql”  --fields-enclosed-by=”#” --fields-optionally-enclosed-by  =”#”</span>

<span class="token comment">--fields-escaped-by</span>
输出文件中的各个字段忽略给定字符。与<span class="token comment">--tab选项一起使用，不能用于--databases和--all-databases选项</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p mysql <span class="token keyword">user</span> <span class="token comment">--tab=”/home/mysql” --fields-escaped-by=”#”</span>

<span class="token comment">--flush-logs</span>
开始导出之前刷新日志。
请注意：假如一次导出多个数据库<span class="token punctuation">(</span>使用选项<span class="token comment">--databases或者--all-databases)，将会逐个数据库刷新日志。除使用--lock-all-tables或者--master-data外。在这种情况下，日志将会被刷新一次，相应的所以表同时被锁定。因此，如果打算同时导出和刷新日志应该使用--lock-all-tables 或者--master-data 和--flush-logs。</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --flush-logs</span>

<span class="token comment">--flush-privileges</span>
在导出mysql数据库之后，发出一条FLUSH  <span class="token keyword">PRIVILEGES</span> 语句。为了正确恢复，该选项应该用于导出mysql数据库和依赖mysql数据库数据的任何时候。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --flush-privileges</span>

<span class="token comment">--force</span>
在导出过程中忽略出现的<span class="token keyword">SQL</span>错误。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --force</span>

<span class="token comment">--help</span>
显示帮助信息并退出。
mysqldump  <span class="token comment">--help</span>

<span class="token comment">--hex-blob</span>
使用十六进制格式导出二进制字符串字段。如果有二进制数据就必须使用该选项。影响到的字段类型有<span class="token keyword">BINARY</span>、<span class="token keyword">VARBINARY</span>、<span class="token keyword">BLOB</span>。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--all-databases --hex-blob</span>

<span class="token comment">--host, -h</span>
需要导出的主机信息
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases</span>

<span class="token comment">--ignore-table</span>
不导出指定表。指定忽略多个表时，需要重复多次，每次一个表。每个表必须同时指定数据库和表名。例如：<span class="token comment">--ignore-table=database.table1 --ignore-table=database.table2 ……</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --ignore-table=mysql.user</span>

<span class="token comment">--include-master-host-port</span>
在<span class="token comment">--dump-slave产生的&#39;CHANGE  MASTER TO..&#39;语句中增加&#39;MASTER_HOST=&lt;host&gt;，MASTER_PORT=&lt;port&gt;&#39;  </span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --include-master-host-port</span>

<span class="token comment">--insert-ignore</span>
在插入行时使用<span class="token keyword">INSERT</span> <span class="token keyword">IGNORE</span>语句<span class="token punctuation">.</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --insert-ignore</span>

<span class="token comment">--lines-terminated-by</span>
输出文件的每行用给定字符串划分。与<span class="token comment">--tab选项一起使用，不能用于--databases和--all-databases选项。</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost test test --tab=”/tmp/mysql”  --lines-terminated-by=”##”</span>

<span class="token comment">--lock-all-tables,  -x</span>
提交请求锁定所有数据库中的所有表，以保证数据的一致性。这是一个全局读锁，并且自动关闭<span class="token comment">--single-transaction 和--lock-tables 选项。</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --lock-all-tables</span>

<span class="token comment">--lock-tables,  -l</span>
开始导出前，锁定所有表。用<span class="token keyword">READ</span>  <span class="token keyword">LOCAL</span>锁定表以允许MyISAM表并行插入。对于支持事务的表例如<span class="token keyword">InnoDB</span>和<span class="token keyword">BDB</span>，<span class="token comment">--single-transaction是一个更好的选择，因为它根本不需要锁定表。</span>
请注意当导出多个数据库时，<span class="token comment">--lock-tables分别为每个数据库锁定表。因此，该选项不能保证导出文件中的表在数据库之间的逻辑一致性。不同数据库表的导出状态可以完全不同。</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --lock-tables</span>

<span class="token comment">--log-error</span>
附加警告和错误信息到给定文件
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases  --log-error=/tmp/mysqldump_error_log.err</span>

<span class="token comment">--max_allowed_packet</span>
服务器发送和接受的最大包长度。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --max_allowed_packet=10240</span>

<span class="token comment">--net_buffer_length</span>
TCP<span class="token operator">/</span>IP和socket连接的缓存大小。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --net_buffer_length=1024</span>

<span class="token comment">--no-autocommit</span>
使用autocommit<span class="token operator">/</span><span class="token keyword">commit</span> 语句包裹表。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --no-autocommit</span>

<span class="token comment">--no-create-db,  -n</span>
只导出数据，而不添加<span class="token keyword">CREATE</span> <span class="token keyword">DATABASE</span> 语句。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --no-create-db</span>

<span class="token comment">--no-create-info,  -t</span>
只导出数据，而不添加<span class="token keyword">CREATE</span> <span class="token keyword">TABLE</span> 语句。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --no-create-info</span>

<span class="token comment">--no-data, -d</span>
不导出任何数据，只导出数据库表结构。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --no-data</span>

<span class="token comment">--no-set-names,  -N</span>
等同于<span class="token comment">--skip-set-charset</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --no-set-names</span>

<span class="token comment">--opt</span>
等同于<span class="token comment">--add-drop-table,  --add-locks, --create-options, --quick, --extended-insert, --lock-tables,  --set-charset, --disable-keys 该选项默认开启,  可以用--skip-opt禁用.</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --opt</span>

<span class="token comment">--order-by-primary</span>
如果存在主键，或者第一个唯一键，对每个表的记录进行排序。在导出MyISAM表到<span class="token keyword">InnoDB</span>表时有效，但会使得导出工作花费很长时间。 
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --order-by-primary</span>

<span class="token comment">--password, -p</span>
连接数据库密码
<span class="token comment">--pipe(windows系统可用)</span>
使用命名管道连接mysql
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --pipe</span>

<span class="token comment">--port, -P</span>
连接数据库端口号

<span class="token comment">--protocol</span>
使用的连接协议，包括：tcp<span class="token punctuation">,</span> socket<span class="token punctuation">,</span> pipe<span class="token punctuation">,</span> memory<span class="token punctuation">.</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --protocol=tcp</span>

<span class="token comment">--quick, -q</span>
不缓冲查询，直接导出到标准输出。默认为打开状态，使用<span class="token comment">--skip-quick取消该选项。</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases </span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --skip-quick</span>

<span class="token comment">--quote-names,-Q</span>
使用（<span class="token punctuation">\`</span>）引起表和列名。默认为打开状态，使用<span class="token comment">--skip-quote-names取消该选项。</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --skip-quote-names</span>

<span class="token comment">--replace</span>
使用<span class="token keyword">REPLACE</span> <span class="token keyword">INTO</span> 取代<span class="token keyword">INSERT</span> <span class="token keyword">INTO</span><span class="token punctuation">.</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --replace</span>

<span class="token comment">--result-file,  -r</span>
直接输出到指定文件中。该选项应该用在使用回车换行对（\\\\r\\\\n）换行的系统上（例如：DOS，Windows）。该选项确保只有一行被使用。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --result-file=/tmp/mysqldump_result_file.txt</span>

<span class="token comment">--routines, -R</span>
导出存储过程以及自定义函数。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --routines</span>

<span class="token comment">--set-charset</span>
添加<span class="token string">&#39;SET NAMES  default_character_set&#39;</span>到输出文件。默认为打开状态，使用<span class="token comment">--skip-set-charset关闭选项。</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases </span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --skip-set-charset</span>

<span class="token comment">--single-transaction</span>
该选项在导出数据之前提交一个<span class="token keyword">BEGIN</span> <span class="token keyword">SQL</span>语句，<span class="token keyword">BEGIN</span> 不会阻塞任何应用程序且能保证导出时数据库的一致性状态。它只适用于多版本存储引擎，仅<span class="token keyword">InnoDB</span>。本选项和<span class="token comment">--lock-tables 选项是互斥的，因为LOCK  TABLES 会使任何挂起的事务隐含提交。要想导出大表的话，应结合使用--quick 选项。</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --single-transaction</span>

<span class="token comment">--dump-date</span>
将导出时间添加到输出文件中。默认为打开状态，使用<span class="token comment">--skip-dump-date关闭选项。</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --skip-dump-date</span>

<span class="token comment">--skip-opt</span>
禁用–opt选项<span class="token punctuation">.</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --skip-opt</span>

<span class="token comment">--socket,-S</span>
指定连接mysql的socket文件位置，默认路径<span class="token operator">/</span>tmp<span class="token operator">/</span>mysql<span class="token punctuation">.</span>sock
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --socket=/tmp/mysqld.sock</span>

<span class="token comment">--tab,-T</span>
为每个表在给定路径创建tab分割的文本文件。注意：仅仅用于mysqldump和mysqld服务器运行在相同机器上。注意使用<span class="token comment">--tab不能指定--databases参数</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost test test --tab=&quot;/home/mysql&quot;</span>

<span class="token comment">--tables</span>
覆盖<span class="token comment">--databases (-B)参数，指定需要导出的表名，在后面的版本会使用table取代tables。</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --databases test --tables test</span>

<span class="token comment">--triggers</span>
导出触发器。该选项默认启用，用<span class="token comment">--skip-triggers禁用它。</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --triggers</span>

<span class="token comment">--tz-utc</span>
在导出顶部设置时区TIME_ZONE<span class="token operator">=</span><span class="token string">&#39;+00:00&#39;</span> ，以保证在不同时区导出的<span class="token keyword">TIMESTAMP</span> 数据或者数据被移动其他时区时的正确性。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --tz-utc</span>

<span class="token comment">--user, -u</span>
指定连接的用户名。

<span class="token comment">--verbose, --v</span>
输出多种平台信息。

<span class="token comment">--version, -V</span>
输出mysqldump版本信息并退出

<span class="token comment">--where, -w</span>
只转储给定的<span class="token keyword">WHERE</span>条件选择的记录。请注意如果条件包含命令解释符专用空格或字符，一定要将条件引用起来。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --where=” user=’root’”</span>

<span class="token comment">--xml, -X</span>
导出XML格式<span class="token punctuation">.</span>
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --xml</span>

<span class="token comment">--plugin_dir</span>
客户端插件的目录，用于兼容不同的插件版本。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --plugin_dir=”/usr/local/lib/plugin”</span>

<span class="token comment">--default_auth</span>
客户端插件默认使用权限。
mysqldump  <span class="token operator">-</span>uroot <span class="token operator">-</span>p <span class="token comment">--host=localhost --all-databases --default-auth=”/usr/local/lib/plugin/&lt;PLUGIN&gt;”</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="导入" tabindex="-1"><a class="header-anchor" href="#导入" aria-hidden="true">#</a> <strong>导入</strong></h2><p><strong>source命令导入</strong></p><div class="language-sql line-numbers-mode" data-ext="sql"><pre class="language-sql"><code>mysql<span class="token operator">&gt;</span><span class="token keyword">use</span> db1
mysql<span class="token operator">&gt;</span>source db1<span class="token punctuation">.</span><span class="token keyword">sql</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><p><strong>mysql命令行导出</strong></p><div class="language-sql line-numbers-mode" data-ext="sql"><pre class="language-sql"><code><span class="token comment">#mysql -uroot -p db1 &lt; db1.sql</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><h2 id="导入去掉指定表" tabindex="-1"><a class="header-anchor" href="#导入去掉指定表" aria-hidden="true">#</a> <strong>导入去掉指定表</strong></h2><p>当导出的SQL脚本文件中存在需要跳过的表数据时，可以在不重新导出的情况下，在sed命令对原脚本文件进行处理，可得到一个更小的导入脚本</p><div class="language-sql line-numbers-mode" data-ext="sql"><pre class="language-sql"><code>sed <span class="token string">&#39;/INSERT INTO \`TABLE1_TO_SKIP\`/d&#39;</span> DBdump<span class="token punctuation">.</span><span class="token keyword">sql</span> <span class="token operator">|</span> \\
sed <span class="token string">&#39;/INSERT INTO \`TABLE2_TO_SKIP\`/d&#39;</span> <span class="token operator">|</span> \\
sed <span class="token string">&#39;/INSERT INTO \`TABLE3_TO_SKIP\`/d&#39;</span> <span class="token operator">&gt;</span> reduced<span class="token punctuation">.</span><span class="token keyword">sql</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,27),t=[o];function p(c,i){return n(),a("div",null,t)}const r=s(l,[["render",p],["__file","mysql-import-export.html.vue"]]);export{r as default};
